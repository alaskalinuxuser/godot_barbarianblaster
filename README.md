# Barbarian Blaster

A simple game designed in Godot while taking a Udemy class.

The game consists of building towers to fend off a hord of barbarians.

The game is not hard to beat, if you survive, you get one star, if you survive without taking any damage to your castle, you get two stars, and if you have over 500 gold when the round ends you get a third star.

This is not a polished game. Just a tool for learning.

The controls are point and click.

Using Godot, this game was built for Linux and Windows, you can download the binaries here:
https://alaskalinuxuser.ddns.net/index.php/s/i23G98wfRsRJMrg
