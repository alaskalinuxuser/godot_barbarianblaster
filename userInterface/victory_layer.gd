extends CanvasLayer

@onready var star_texture_rect: TextureRect = %StarTextureRect
@onready var star_texture_rect_2: TextureRect = %StarTextureRect2
@onready var star_texture_rect_3: TextureRect = %StarTextureRect3
@onready var health_label: Label = %healthLabel
@onready var money_label: Label = %moneyLabel

@onready var money = get_tree().get_first_node_in_group("bank")
@onready var base = get_tree().get_first_node_in_group("base")

func restart_game() -> void:
	get_tree().reload_current_scene()

func quit_game() -> void:
	get_tree().quit()

func victory() -> void:
	visible = true
	if base.max_health == base.current_health:
		star_texture_rect_2.modulate = Color.WHITE
		health_label.visible = true
	if money.gold > 500:
		star_texture_rect_3.modulate = Color.WHITE
		money_label.visible = true

