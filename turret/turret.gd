extends Node3D

@export var projectile : PackedScene
@export var turret_range: float = 10.0
var enemy_path: Path3D
var target: PathFollow3D
@onready var fire_animation: AnimationPlayer = $FireAnimation
@onready var cannon_front: Node3D = $TurretBase/TurretTop/Cannon
@onready var firing_point: Node3D = $TurretBase/TurretTop/Cannon/FiringPoint
@onready var shot_stream_player: AudioStreamPlayer = $shotStreamPlayer

func _physics_process(_delta: float) -> void:
	target = find_best_target()
	if target:
		cannon_front.look_at(target.global_position, Vector3.UP, true)
	

func _on_timer_timeout() -> void:
	if target:
		var shot = projectile.instantiate()
		firing_point.add_child(shot)
		shot.global_position = firing_point.global_position
		shot.direction = firing_point.global_transform.basis.z
		fire_animation.play("Fire")
		shot_stream_player.play()
	

func find_best_target() -> PathFollow3D:
	var best_target=null
	var best_progress = 0
	for enemy in enemy_path.get_children():
		if enemy is PathFollow3D:
			if enemy.progress > best_progress:
				var distance := global_position.distance_to(enemy.global_position)
				if distance < turret_range:
					best_progress = enemy.progress
					best_target = enemy
		
	return best_target
