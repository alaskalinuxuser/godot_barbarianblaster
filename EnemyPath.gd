extends Path3D

@onready var enemy_path_timer: Timer = $EnemyPathTimer
@onready var victory_stream_player: AudioStreamPlayer = $"../VictoryStreamPlayer"

@export var enemyScene: PackedScene
@export var difficulty_manager : Node
@export var victory_layer : CanvasLayer

func spawn_enemy() -> void:
	var newEnemy = enemyScene.instantiate()
	newEnemy.max_health = difficulty_manager.get_enemy_health()
	add_child(newEnemy)
	enemy_path_timer.wait_time = difficulty_manager.get_spawn_time()
	#print(newEnemy.current_health)
	newEnemy.tree_exited.connect(enemy_defeated)


func _on_enemy_path_timer_timeout() -> void:
	spawn_enemy()


func _on_difficulty_manager_stop_spawning_enemies() -> void:
	enemy_path_timer.stop()

func enemy_defeated() -> void:
	if enemy_path_timer.is_stopped():
		for child in get_children():
			if child is PathFollow3D:
				return
		victory_layer.victory()
		if victory_stream_player.playing != true:
			victory_stream_player.play()
