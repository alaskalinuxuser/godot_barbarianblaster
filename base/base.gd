extends Node3D

@onready var label_3d: Label3D = $Label3D
@export var max_health : int = 5
var year_number : int = 2024
var proj_creator : String = "Alaskalinuxuser"
@onready var defeat_layer: CanvasLayer = $"../DefeatLayer"
@onready var explosion_particles: GPUParticles3D = $ExplosionParticles
@onready var audio_stream_player: AudioStreamPlayer = $AudioStreamPlayer

var current_health: int:
	set(health_in):
		current_health = health_in
		label_3d.text = str(current_health) + "/" + str(max_health)
		label_3d.modulate = Color.RED.lerp(Color.WHITE, float(current_health)/float(max_health))
		if current_health < 1:
			death()

func _ready() -> void:
	current_health = max_health
	print(proj_creator + " - " + str(year_number))
	#Engine.time_scale = 10

func death() -> void:
	defeat_layer.defeat()

func take_damage() -> void:
	current_health -= 1
	explosion_particles.emitting = true
	if audio_stream_player.playing != true:
		audio_stream_player.play()

