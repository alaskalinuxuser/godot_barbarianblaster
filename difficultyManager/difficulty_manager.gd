extends Node

signal stop_spawning_enemies

@onready var timer: Timer = $Timer
@export var gamelength := 30.0
@export var spawnTimeCurve : Curve
@export var EnemyHealthTimeCurve : Curve


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	timer.start(gamelength)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta: float) -> void:
	pass

func game_progress_ratio() -> float:
	return 1.0 - (timer.time_left / gamelength)

func get_spawn_time() -> float:
	return spawnTimeCurve.sample(game_progress_ratio())

func get_enemy_health() -> int:
	return int(EnemyHealthTimeCurve.sample(game_progress_ratio()))

func _on_timer_timeout() -> void:
	stop_spawning_enemies.emit()
